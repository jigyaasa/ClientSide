import { Component, OnInit } from '@angular/core';
import { RouterModule, Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms'
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { VerificationModal } from './../Verification-modal/verification-modal.component'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  protected formdata;  
  protected errorMessage: string = "";
  protected successMessage: string = "";

  constructor(private router: Router, private modalService: NgbModal) { }

  ngOnInit() {
    this.formdata = new FormGroup({
      fname: new FormControl(""),
      mname: new FormControl(""),
      lname: new FormControl(""),
      cid: new FormControl(""),
      uname: new FormControl(""),
      passwd: new FormControl(""),
      contactNumber: new FormControl(""),
      email: new FormControl(""),
      address: new FormControl(""),
      upload: new FormControl("")
    });
  }

  onClickSubmit(data) {
    console.log("Inside submit button " + JSON.stringify(data));
    if(data.uname === "" && data.passwd === "")  {
      setTimeout(() => {
        this.errorMessage = ""
      }, 3000);
      this.errorMessage = "Username and Password should not be empty"
    } else {
      this.open();
    }
  }   
  open() {
    const modalRef = this.modalService.open(VerificationModal);
    modalRef.componentInstance.name = 'World';
  }

  onSignIn(){
    this.router.navigate(['login']);
  }

}
