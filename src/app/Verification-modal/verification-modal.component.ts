import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms'
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngbd-modal-content',
  templateUrl: './verification-modal.component.html',
  styleUrls: ['./verification-modal.component.css']
})

export class VerificationModal implements OnInit {
  protected formdata;

  @Input() name;
  
  constructor(public activeModal: NgbActiveModal) {}
  
  ngOnInit() {
    this.formdata = new FormGroup({
      secret: new FormControl(""),
    });
  }

  onClickSubmit(data) {
    console.log("Inside submit button " + JSON.stringify(data))
  }

}