import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './core/app.routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { UpdateDetailsComponent } from './Update-details/update-details.component';
import { UpdateDetailsService } from './Update-details/update-details.service';
import { RegisterComponent } from './Register/register.component';
import { RegisterService } from './Register/register.service';
import { LoginComponent } from './login/login.component';
import { LoginService } from './login/login.service';
import { VerificationModal } from './Verification-modal/verification-modal.component'


@NgModule({
  declarations: [
    AppComponent,
    UpdateDetailsComponent,
    RegisterComponent,
    LoginComponent,
    VerificationModal
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    HttpModule
  ],
  providers: [
    LoginService,
    RegisterService,
    UpdateDetailsService
  ],
  entryComponents: [
    VerificationModal
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
