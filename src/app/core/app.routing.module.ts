import { NgModule, ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'
import { LoginComponent } from './../login/login.component'
import { RegisterComponent } from './../Register/register.component'
import { UpdateDetailsComponent } from './../Update-details/update-details.component'

const routes: ModuleWithProviders = RouterModule.forRoot([
    { path: '', component: LoginComponent },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'updateInfo', component: UpdateDetailsComponent },
  ], { useHash: false});

  @NgModule({
    imports: [routes],
    exports: [RouterModule],
    providers : []
  })
  export class AppRoutingModule { }