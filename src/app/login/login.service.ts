import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
 
@Injectable()
export class LoginService {
    // public url:string = "http://ec2-18-218-131-46.us-east-2.compute.amazonaws.com/"/
    constructor(private http: Http) { }
    
    checkLoginDeatils(body): Observable<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        // let headers = new Headers({ 'Content-Type': 'application/json' });
        // headers.append('Content-Type', 'application/x-www-form-urlencoded');
        let options = new RequestOptions({ headers: headers });
        return  this.http.post("http://ec2-18-218-131-46.us-east-2.compute.amazonaws.com/login", body, { headers: headers }) 
            .map(res => res)
    }

}